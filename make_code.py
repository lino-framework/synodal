#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# Copyright 2019-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
Generates the synodal.py file.
"""

__version__ = '24.11.0'

# import os
# from importlib import import_module, metadata
from importlib import import_module
import click
import datetime

from pathlib import Path
from pprint import pformat

import requests
from bs4 import BeautifulSoup
from importlib import metadata

INTRO_CODE = """
from collections import namedtuple

repo_fields = ("nickname", "package_name", "git_repo", "settings_module",
    "front_end", "extra_deps", "public_url", "verbose_name", "description")
Repository = namedtuple('Repository',
                        repo_fields,
                        defaults=['', '', '', [], '', None, None])
PublicSite = namedtuple('PublicSite', "url settings_module default_ui")

"""

exec(INTRO_CODE)

# print(help(Repository))

REPOS_DICT = {}
REPOS_LIST = []
PUBLIC_SITES = []


def add(*args, **kwargs):
    # values = {repo_fields[i]:v for i, v in enumerate(args)}

    assert repo_fields[3] == "settings_module"
    settings_module = args[3] if len(args) > 3 else None
    pkgname = args[1]

    if settings_module:
        if "public_url" in kwargs:
            r = requests.get(kwargs["public_url"])
            r.encoding = "utf-8"  # educated guess fails
            soup = BeautifulSoup(r.text, "lxml")
            desc = soup.find("title").text.split("—")[0]
            lst = desc.split(":")
            if len(lst) == 2:
                desc = lst[1].strip()
            kwargs["description"] = desc
        else:
            m = import_module(settings_module)
            s = m.Site
            kwargs["verbose_name"] = s.verbose_name
            if (summary := s.description) is None and pkgname:
                summary = metadata.metadata(pkgname)["Summary"]
            kwargs["description"] = summary

    r = Repository(*args, **kwargs)

    REPOS_LIST.append(r)
    REPOS_DICT[r.nickname] = r
    if r.front_end:
        # add an alias because front ends are looked up using their full package name
        REPOS_DICT[r.front_end] = r


# tools to be installed with --clone because they are required for a complete contributor environment:
# add("synodal", "synodal", "https://gitlab.com/lino-framework/synodal.git")

add(
    "atelier",
    "atelier",
    "https://gitlab.com/lino-framework/atelier.git",
    public_url="https://atelier.lino-framework.org",
)
# add("rstgen", "rstgen", "https://gitlab.com/lino-framework/rstgen.git")
add(
    "etgen",
    "etgen",
    "https://github.com/lino-framework/etgen",
    public_url="https://etgen.lino-framework.org",
)
add("eid", "eidreader", "https://github.com/lino-framework/eidreader")

add("cd", "commondata", "https://github.com/lsaffre/commondata")
# add("be", "commondata.be", "https://github.com/lsaffre/commondata-be")
# add("ee", "commondata.ee", "https://github.com/lsaffre/commondata-ee")
# add("eg", "commondata.eg", "https://github.com/lsaffre/commondata-eg")

add(
    "getlino",
    "getlino",
    "https://gitlab.com/lino-framework/getlino.git",
    public_url="https://getlino.lino-framework.org",
)
add(
    "lino",
    "lino",
    "https://gitlab.com/lino-framework/lino.git",
    "",
    "lino.modlib.extjs",
)
add("xl", "lino-xl", "https://gitlab.com/lino-framework/xl.git")
add(
    "welfare",
    "lino-welfare",
    "https://gitlab.com/lino-framework/welfare.git",
    public_url="https://welfare.lino-framework.org",
)

add(
    "react",
    "lino-react",
    "https://gitlab.com/lino-framework/react.git",
    "",
    "lino_react.react",
    public_url="https://react.lino-framework.org",
)
add(
    "openui5",
    "lino-openui5",
    "https://gitlab.com/lino-framework/openui5.git",
    "",
    "lino_openui5.openui5",
)

add(
    "book",
    "lino-book",
    "https://gitlab.com/lino-framework/book.git",
    public_url="https://dev.lino-framework.org/",
)
add(
    "cg",
    "",
    "https://gitlab.com/lino-framework/cg.git",
    public_url="https://community.lino-framework.org/",
)
add(
    "ug",
    "",
    "https://gitlab.com/lino-framework/ug.git",
    public_url="https://using.lino-framework.org/",
)
add(
    "hg",
    "",
    "https://gitlab.com/lino-framework/hg.git",
    public_url="https://hosting.lino-framework.org/",
)
add(
    "lf",
    "",
    "https://gitlab.com/lino-framework/lf.git",
    public_url="https://www.lino-framework.org/",
)
add(
    "ss",
    "",
    "https://gitlab.com/synodalsoft/ss.git",
    public_url="https://www.synodalsoft.net/",
)

add("algus", "", "https://gitlab.com/lino-framework/algus.git")

# demo projects of the book can be used by getlino startsite as "applications"
# that have no repo on their own

add("min1", "", "", "lino_book.projects.min1.settings")
add("min2", "", "", "lino_book.projects.min2.settings")
# add("polls", "", "", "lino_book.projects.polls.mysite.settings")
add(
    "cosi4",
    "",
    "",
    "lino_book.projects.cosi4.settings",
    public_url="https://dev.lino-framework.org/projects/cosi4.html",
)
add(
    "cosi5",
    "",
    "",
    "lino_book.projects.cosi5.settings",
    public_url="https://dev.lino-framework.org/projects/cosi5.html",
)
add(
    "tera2",
    "",
    "",
    "lino_book.projects.voga2.settings",
    public_url="https://dev.lino-framework.org/projects/voga2.html",
)
add(
    "cosi3",
    "",
    "",
    "lino_book.projects.cosi3.settings",
    public_url="https://dev.lino-framework.org/projects/cosi3.html",
)
add(
    "cosi2",
    "",
    "",
    "lino_book.projects.cosi2.settings.demo",
    public_url="https://dev.lino-framework.org/projects/cosi2.html",
)
add(
    "cosi1",
    "",
    "",
    "lino_book.projects.cosi1.settings.demo",
    public_url="https://dev.lino-framework.org/projects/cosi1.html",
)
add(
    "tera1",
    "",
    "",
    "lino_book.projects.tera1.settings.demo",
    public_url="https://dev.lino-framework.org/projects/tera1.html",
)
add(
    "noi1r",
    "",
    "",
    "lino_book.projects.noi1r.settings",
    public_url="https://dev.lino-framework.org/projects/noi1r.html",
)
add(
    "chatter",
    "",
    "",
    "lino_book.projects.chatter.settings",
    public_url="https://dev.lino-framework.org/projects/chatter.html",
)
add(
    "polly",
    "",
    "",
    "lino_book.projects.polly.settings.demo",
    public_url="https://dev.lino-framework.org/projects/polly.html",
)

# e.g. for installing a non-Lino site like mailman
# add("std", "", "", "lino.projects.std.settings")


def add2(*args, **kwargs):
    kwargs.update(extra_deps="lino xl".split())
    add(*args, **kwargs)


add2(
    "amici",
    "lino-amici",
    "https://gitlab.com/lino-framework/amici.git",
    "lino_amici.lib.amici.settings",
)
add2(
    "avanti",
    "lino-avanti",
    "https://gitlab.com/lino-framework/avanti.git",
    "lino_avanti.lib.avanti.settings",
)
add2(
    "cms",
    "lino-cms",
    "https://gitlab.com/lino-framework/cms.git",
    "lino_cms.lib.cms.settings",
)
add(
    "prima",
    "lino-prima",
    "https://gitlab.com/synodalsoft/prima.git",
    "lino_prima.lib.prima.settings",
    extra_deps=["lino"]
)
add2(
    "care",
    "lino-care",
    "https://gitlab.com/lino-framework/care.git",
    "lino_care.lib.care.settings",
)
add2(
    "cosi",
    "lino-cosi",
    "https://gitlab.com/lino-framework/cosi.git",
    "lino_cosi.lib.cosi.settings",
)
add2(
    "mentori",
    "lino-mentori",
    "https://gitlab.com/lino-framework/mentori.git",
    "lino_mentori.lib.mentori.settings",
)
add2(
    "noi",
    "lino_noi",
    "https://gitlab.com/lino-framework/noi.git",
    "lino_noi.lib.noi.settings",
)
add2(
    "presto",
    "lino-presto",
    "https://gitlab.com/lino-framework/presto.git",
    "lino_presto.lib.presto.settings",
)
add2(
    "pronto",
    "lino-pronto",
    "https://gitlab.com/lino-framework/pronto.git",
    "lino_pronto.lib.pronto.settings",
)
add2(
    "tera",
    "lino-tera",
    "https://gitlab.com/lino-framework/tera.git",
    "lino_tera.lib.tera.settings",
)
add2(
    "shop",
    "lino-shop",
    "https://gitlab.com/lino-framework/shop.git",
    "lino_shop.lib.shop.settings",
)
add2(
    "vilma",
    "lino-vilma",
    "https://gitlab.com/lino-framework/vilma.git",
    "lino_vilma.lib.vilma.settings",
)
add2(
    "voga",
    "lino-voga",
    "https://gitlab.com/lino-framework/voga.git",
    "lino_voga.lib.voga.settings",
)
# add2("ciao", "lino-ciao", "https://github.com/lino-framework/ciao", "lino_ciao.lib.ciao.settings")


def add2(*args, **kwargs):
    kwargs.update(extra_deps="lino xl welfare".split())
    add(*args, **kwargs)


add2(
    "weleup",
    "lino-weleup",
    "https://gitlab.com/lino-framework/weleup.git",
    "lino_weleup.settings",
)
add2(
    "welcht",
    "lino-welcht",
    "https://gitlab.com/lino-framework/welcht.git",
    "lino_welcht.settings",
)


def add(*args, **kwargs):
    ps = PublicSite(*args, **kwargs)
    PUBLIC_SITES.append(ps)


add(
    "https://voga1e.lino-framework.org",
    "lino_voga.lib.voga.settings",
    "lino.modlib.extjs",
)
add(
    "https://voga1r.lino-framework.org",
    "lino_voga.lib.voga.settings",
    "lino_react.react",
)
add(
    "https://cosi1e.lino-framework.org",
    "lino_cosi.lib.cosi.settings",
    "lino_react.react",
)
add("https://noi1r.lino-framework.org", "lino_noi.lib.noi.settings", "lino_react.react")
add("https://weleup1.mylino.net", "lino_weleup.settings", "lino.modlib.extjs")
add("https://welcht1.mylino.net", "lino_welcht.settings", "lino.modlib.extjs")

SPHINX_EXTLINKS = {
    "ticket": ("https://jane.mylino.net/#/api/tickets/PublicTickets/%s", "#%s")
}


REPOS_LIST = pformat(REPOS_LIST)
PUBLIC_SITES = pformat(PUBLIC_SITES)
SPHINX_EXTLINKS = pformat(SPHINX_EXTLINKS)


def write_synodal_py(batch):
    # __version__ = metadata.version("synodal")

    content = f"""\
#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# code generated {datetime.datetime.now()} by make_code.py script of synodal
# fmt: off

{INTRO_CODE}

__version__ = '{__version__}'

KNOWN_REPOS = REPOS_LIST = {REPOS_LIST}

REPOS_DICT = {{r.nickname: r for r in REPOS_LIST}}

FRONT_ENDS = {{r.front_end: r for r in KNOWN_REPOS if r.front_end}}

PUBLIC_SITES = {PUBLIC_SITES}

SPHINX_EXTLINKS = {SPHINX_EXTLINKS}

# end of generated code
"""

    filename = Path(__file__).parent / "synodal.py"
    if filename.exists() and not batch:
        msg = f"OK to overwrite {filename} ?"
        click.echo(msg + " [Y/n]", nl=False)
        c = click.getchar()
        click.echo(c)
        if c in "nN":
            print(content)
            return
    click.echo(f"Write {filename}...")

    with open(filename, encoding="utf-8", mode="w") as f:
        f.write(content)


@click.command()
@click.option(
    "--batch",
    "-b",
    default=False,
    help="Whether to suppress any interactive questions.",
)
def main(batch):
    write_synodal_py(batch)


if __name__ == "__main__":
    main()
